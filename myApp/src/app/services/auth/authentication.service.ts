import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController, Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { MenuController } from '@ionic/angular';
import { RestService } from '../rest/rest.service';



@Injectable()
export class AuthenticationService {

    authState = new BehaviorSubject(false);

    constructor(
        private router: Router,
        private storage: Storage,
        private platform: Platform,
        public toastController: ToastController,
        public rest: RestService,
        private menuCtrl: MenuController,
    ) {
        this.platform.ready().then(() => {
            this.ifLoggedIn();
        });
    }

    ifLoggedIn() {
        this.storage.get('USER_INFO').then((response) => {
            if (response) {
                this.authState.next(true);
            }
        });
    }


    login(id, tipo, email) {

        this.rest.authenticate(id, tipo, email).then(data => {
            console.log(data);
            var dummy_response = {
                user_login: data['user_login'],
                token: data['token'],
                type : data['type']
            };

            this.storage.set('USER_INFO', dummy_response).then((response) => {
                if(response.type == "1"){
                    this.menuCtrl.enable(true, 'admin');
                    this.menuCtrl.enable(false, 'first');
                } else {
                    this.menuCtrl.enable(true, 'first');
                    this.menuCtrl.enable(false, 'admin');
                }
                
                this.router.navigate(['home']);
                this.authState.next(true);
            });

            //this.app.addUserItem();

        });

    }

    logout() {
        this.storage.remove('USER_INFO').then(() => {
            this.menuCtrl.enable(false);
            this.router.navigate(['login']);
            this.authState.next(false);
        });
    }

    isAuthenticated() {
        return this.authState.value;
    }



}