import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor(public alertController: AlertController, private router: Router) { }

    async normalAlert(text) {
        const alert = await this.alertController.create({
            header: '¡Atención!',
            message: text,
            buttons: [
                {
                    text: 'OK',
                    handler: () => {
                        this.router.navigate(['home'], { replaceUrl: true });
                    }
                }
            ]
        });
        await alert.present();
    }

    async normalAlertNoHome(text) {
        const alert = await this.alertController.create({
            header: '¡Atención!',
            message: text,
            buttons: [
                {
                    text: 'OK',                    
                }
            ]
        });
        await alert.present();
    }

    async normalAlertWithReturn(text, name) {
        const alert = await this.alertController.create({
            header: '¡Atención!',
            message: text,
            buttons: [
                {
                    text: 'OK',  
                    handler: () => {
                        this.router.navigate([name], { replaceUrl: true });
                    }                  
                }
            ]
        });
        await alert.present();
    }

}
