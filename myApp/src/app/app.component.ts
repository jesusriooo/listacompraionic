import { Component, NgZone } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router, NavigationExtras, RouterEvent } from '@angular/router';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { ProductPricesPage } from './pages/product-prices/product-prices.page';
import { AuthenticationService } from './services/auth/Authentication.service';
import { Storage } from '@ionic/storage';


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {

    navigate: any;
    stop = false;

    constructor(private storage: Storage, private zone: NgZone, private deeplinks: Deeplinks, private router: Router, private authenticationService: AuthenticationService, private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar) {
        this.sideMenu();
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.deepLinks();
            this.statusBar.backgroundColorByHexString('#cf3c4f');
            //setTimeout(() => {this.checkLogin()}, 5000);
            this.checkLogin();
            this.splashScreen.hide();

        });
    }

    delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    checkLogin(){
        this.authenticationService.authState.subscribe(state => {
            if (!this.stop) {
                if (state) {
                    this.router.navigate(['home']);
                } else {
                    this.router.navigate(['login']);
                }
            }
        });
    }

    deepLinks() {

        this.deeplinks.route({
            '/product-prices/:ean': ProductPricesPage,
        }).subscribe((match) => {
            // match.$route - the route we matched, which is the matched entry from the arguments to route()
            // match.$args - the args passed in the link
            // match.$link - the full link data   
            this.stop = true;         
            this.zone.run(() => {
                let navigationExtras: NavigationExtras = {
                    state: {
                        ean: match.$args.ean,
                    }
                };
                this.router.navigate(['/product-prices'], navigationExtras).then(success => {
                    console.log("Voy a productos. -> " + success, Date() );
                    if(!success){
                        this.stop = false;
                        this.checkLogin();
                    }
                });
            });

        }, (nomatch) => {
            // nomatch.$link - the full link data             
            console.error('Got a deeplink that didn\'t match', nomatch);
        });

    }

    sideMenu() {
        this.navigate =
            [
                {
                    title: "Home",
                    url: "/home",
                    icon: "home"
                }, {
                    title: "Historial de búsqueda",
                    url: "/historial",
                    icon: "timer"
                }, {
                    title: "Favoritos",
                    url: "/favoritos",
                    icon: "star"
                },  {
                    title: "Buscar productos",
                    url: "/busqueda",
                    icon: "search"
                }, {
                    title: "Alternativas",
                    url: "/alternativas",
                    icon: "code-working-outline"
                }
            ]
            
    }

    logout() {
        this.authenticationService.logout();
    }

}
