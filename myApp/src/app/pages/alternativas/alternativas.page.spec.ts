import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AlternativasPage } from './alternativas.page';

describe('AlternativasPage', () => {
  let component: AlternativasPage;
  let fixture: ComponentFixture<AlternativasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlternativasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AlternativasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
