import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductPricesPage } from './product-prices.page';

const routes: Routes = [
  {
    path: '',
    component: ProductPricesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductPricesPageRoutingModule {}
