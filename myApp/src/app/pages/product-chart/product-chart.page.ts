import { Component, OnInit, ViewChildren, QueryList, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Chart } from 'chart.js';
import { Platform } from '@ionic/angular';
import { isNumber } from 'util';
import { RestService } from 'src/app/services/rest/rest.service';
import { AlertService } from 'src/app/services/alert/alert.service';


@Component({
    selector: 'app-product-chart',
    templateUrl: './product-chart.page.html',
    styleUrls: ['./product-chart.page.scss'],
})
export class ProductChartPage implements OnInit {

    @ViewChildren('list') list: QueryList<ElementRef>;
    data: any;
    json: any;
    bars: any;
    subscription: any;

    constructor(private platform: Platform, public rest: RestService, public alert: AlertService, private route: ActivatedRoute, private router: Router, ) {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.data = this.router.getCurrentNavigation().extras.state.ean;
                this.getProductChart(false);
                //console.log(this.data);
            } else {
                this.alert.normalAlert('No se ha recibido el código de barras, intentelo de nuevo.');
            }
        });
    }

    doRefresh(event) {
        this.getProductChart(event);
    }

    getProductChart(event) {
        this.rest.getProductChart(this.data).then(data => {
            this.json = data;
            if (event !== false)
                event.target.complete();
            //console.log("->", data);
            this.list.changes.subscribe(() => {
                this.list.toArray().forEach((el, index) => {
                    this.createBarChart(el, index);
                });
            });
        });
    }

    ionViewDidEnter() {
        // Función que saca de la aplicación cuando se pulsa el botón
        // de atrás en la pantalla home.
        this.subscription = this.platform.backButton.subscribe(() => {
            let navigationExtras: NavigationExtras = {
                state: {
                    ean: this.data
                }
            };
            //console.log("PULSADO BOTÓN ATRÁS DESDE CHART");
            this.router.navigate(['product-prices'], navigationExtras);
        });
    }

    ionViewWillLeave() {
        // Cancela la subscripción al botón atrás
        this.subscription.unsubscribe();
    }

    createBarChart(element, index) {

        var max = Math.max.apply(Math, this.json[index].precios);
        if(isNumber(max)){
            max = (max + max);
        } else {
            max = 1;
        }
        this.bars = new Chart(element.el.lastElementChild.firstElementChild, {
            type: 'line',
            data: {
                labels: this.json[index].fechas,
                datasets: [{
                    data: this.json[index].precios,
                    backgroundColor: "#faced4",
                    borderColor: getComputedStyle(document.documentElement).getPropertyValue('--ion-color-danger'),
                    borderWidth: 1
                }]
            }, options: {
                legend: {
                    display: false,
                }, scales: {
                    yAxes: [{
                        ticks: {
                                beginAtZero: true,
                                max : max
                            }
                        }
                    ]
                }
            }
        });
    }

    ngOnInit() {
    }

}
