import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/auth/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'product-prices',
    loadChildren: () => import('./pages/product-prices/product-prices.module').then(m => m.ProductPricesPageModule)
  },
  {
    path: 'historial',
    loadChildren: () => import('./pages/historial/historial.module').then(m => m.HistorialPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'product-chart',
    loadChildren: () => import('./pages/product-chart/product-chart.module').then( m => m.ProductChartPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'favoritos',
    loadChildren: () => import('./pages/favoritos/favoritos.module').then( m => m.FavoritosPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'lista-compra',
    loadChildren: () => import('./pages/lista-compra/lista-compra.module').then( m => m.listaCompraPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'busqueda',
    loadChildren: () => import('./pages/busqueda/busqueda.module').then( m => m.BusquedaPageModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./pages/users/users.module').then( m => m.UsersPageModule)
  },
  {
    path: 'alert-modal',
    loadChildren: () => import('./pages/alert-modal/alert-modal.module').then( m => m.AlertModalPageModule)
  },
  {
    path: 'alternativas',
    loadChildren: () => import('./pages/alternativas/alternativas.module').then( m => m.AlternativasPageModule)
  },
  {
    path: 'alternativas-new',
    loadChildren: () => import('./pages/alternativas-new/alternativas-new.module').then( m => m.AlternativasNewPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
