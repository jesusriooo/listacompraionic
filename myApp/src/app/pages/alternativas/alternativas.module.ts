import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlternativasPageRoutingModule } from './alternativas-routing.module';

import { AlternativasPage } from './alternativas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlternativasPageRoutingModule
  ],
  declarations: [AlternativasPage]
})
export class AlternativasPageModule {}
