import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductChartPage } from './product-chart.page';

describe('ProductChartPage', () => {
  let component: ProductChartPage;
  let fixture: ComponentFixture<ProductChartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductChartPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductChartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
