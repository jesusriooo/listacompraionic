
ionic cordova build android --release --versionCode=3

cd .\platforms\android\

.\gradlew bundle

cd ..\..\

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore .\ahorrarCompra.keystore .\platforms\android\app\build\outputs\bundle\release\app.aab ahorraCompra