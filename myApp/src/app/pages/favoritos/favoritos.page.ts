import { Component } from '@angular/core';

import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Router, NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { RestService } from 'src/app/services/rest/rest.service';

@Component({
    selector: 'app-favoritos',
    templateUrl: './favoritos.page.html',
    styleUrls: ['./favoritos.page.scss'],
})
export class FavoritosPage {

    json: any;
    subscription: any;

    constructor(private storage: Storage, private router: Router, private photoViewer: PhotoViewer, public rest: RestService, ) {

    }

    getFavoritos(event) {

        this.storage.get('USER_INFO').then((rs) => {
            this.rest.getFavoritos(rs.token).then(data => {
                this.json = data;
                if (event !== false)
                    event.target.complete();
            });
        });


    }

    seeFullImage(url, nombre) {
        this.photoViewer.show(url, nombre, { share: true });
    }

    goToProductInfo(ean) {
        let navigationExtras: NavigationExtras = {
            state: {
                ean: ean
            }
        };
        this.router.navigate(['product-prices'], navigationExtras);
    }

    doRefresh(event) {
        this.getFavoritos(event);
    }

    ionViewWillEnter() {
        this.getFavoritos(false);
    }

}
