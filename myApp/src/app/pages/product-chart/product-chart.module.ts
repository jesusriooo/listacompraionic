import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductChartPageRoutingModule } from './product-chart-routing.module';

import { ProductChartPage } from './product-chart.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductChartPageRoutingModule
  ],
  declarations: [ProductChartPage]
})
export class ProductChartPageModule {}
