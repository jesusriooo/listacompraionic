import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { listaCompraPageRoutingModule } from './lista-compra-routing.module';

import { listaCompraPage } from './lista-compra.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        listaCompraPageRoutingModule
    ],
    declarations: [listaCompraPage]
})
export class listaCompraPageModule { }
