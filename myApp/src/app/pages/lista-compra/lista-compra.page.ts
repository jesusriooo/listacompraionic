import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { PopoverController, AlertController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert/alert.service';
import { EditProductInListComponent } from 'src/app/component/edit-product-in-list/edit-product-in-list.component';
import { RestService } from 'src/app/services/rest/rest.service';

@Component({
    selector: 'app-lista-compra',
    templateUrl: './lista-compra.page.html',
    styleUrls: ['./lista-compra.page.scss'],
})
export class listaCompraPage implements OnInit {

    json: any;
    data: any;
    result : any;

    constructor(private alertCtrl: AlertController, public popoverController: PopoverController, public alert: AlertService, private route: ActivatedRoute, private router: Router, private storage: Storage, public rest: RestService) {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.data = this.router.getCurrentNavigation().extras.state.name;
                this.getProductoOfList(false, this.data);
            } else if (this.data != '') {

            } else {
                this.alert.normalAlert('No se ha recibido el nombre, intentelo de nuevo.');
            }
        });

    }

    async borrarListaConfirm(name) {
        const alert = await this.alertCtrl.create({
            header: 'Borrar lista',
            message: '¿Deseas realmente borrar la lista de la compra?',
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel'
                },
                {
                    text: 'Borrar',
                    handler: data => {
                        this.borrarLista(name);
                    }
                }
            ]
        });
        await alert.present();
    }

    borrarLista(name) {
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.borrarLista(rs.token, name).then(d => {
                this.result = d;
                if (this.result.error) {
                    this.alert.normalAlert(this.result.error.text);
                } else if (this.result.ok) {
                    this.alert.normalAlert(this.result.ok.text);
                } else {

                }
            });
        });
    }

    async presentPopover(ean, nombre, myEvent) {
        const popover = await this.popoverController.create({
            component: EditProductInListComponent,
            componentProps: { ean: ean, listaCompra: nombre },
            translucent: true,
            event: myEvent
        });

        popover.onDidDismiss().then((data : any) => {
            if(data.role != "backdrop")
                this.getProductoOfList(false, this.data, true);
        });

        return await popover.present();
    }

    getProductoOfList(event, name, loading = true) {
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.getProductoOfList(rs.token, name, loading).then(data => {
                this.json = data;
                if (event !== false)
                    event.target.complete();
            });
        });
    }

    doRefresh(event) {
        this.getProductoOfList(event, this.data);
    }

    goToProductInfo(ean) {
        let navigationExtras: NavigationExtras = {
            state: {
                ean: ean
            }
        };
        this.router.navigate(['product-prices'], navigationExtras);
    }

    ngOnInit() {
    }

}
