import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlternativasNewPageRoutingModule } from './alternativas-new-routing.module';

import { AlternativasNewPage } from './alternativas-new.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlternativasNewPageRoutingModule
  ],
  declarations: [AlternativasNewPage]
})
export class AlternativasNewPageModule {}
