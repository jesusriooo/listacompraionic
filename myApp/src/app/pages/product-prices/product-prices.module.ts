import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductPricesPageRoutingModule } from './product-prices-routing.module';

import { ProductPricesPage } from './product-prices.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductPricesPageRoutingModule
  ],
  declarations: [ProductPricesPage]
})
export class ProductPricesPageModule {}
