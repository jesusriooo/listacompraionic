import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { listaCompraPage } from './lista-compra.page';

describe('listaCompraPage', () => {
  let component: listaCompraPage;
  let fixture: ComponentFixture<listaCompraPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ listaCompraPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(listaCompraPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
