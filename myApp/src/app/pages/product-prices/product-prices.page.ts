import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { ToastController, ActionSheetController, AlertController, PopoverController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert/alert.service';
import { RestService } from 'src/app/services/rest/rest.service';
import { AddProductInListComponent } from 'src/app/component/add-product-in-list/add-product-in-list.component';



@Component({
    selector: 'app-product-prices',
    templateUrl: './product-prices.page.html',
    styleUrls: ['./product-prices.page.scss'],
})
export class ProductPricesPage {

    data: any;
    json: any;
    listas: any;
    resultFavorite: any;
    resultAdd: any;
    result: any;
    buttons = [];
    masBarato : any;
    slideOpts: any;
    nMaxReload = 5;
    nReload = 0;
    showSpinner = false;

    constructor(public popoverController: PopoverController, private alertCtrl: AlertController, private actionSheetController: ActionSheetController, public toastController: ToastController, private socialSharing: SocialSharing, private storage: Storage, private photoViewer: PhotoViewer, public alert: AlertService, public rest: RestService, private route: ActivatedRoute, private router: Router) {
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.data = this.router.getCurrentNavigation().extras.state.ean;
                this.cargarProductos(false);
            } else if (this.data != '') {

            } else {
                this.alert.normalAlert('No se ha recibido el código de barras, intentelo de nuevo.');
            }
        });
        this.createButtons();
        this.slideOpts = {
            slidesPerView: 1,
            spaceBetween : -13
          }
    }

    goToProductInfo(ean) {
        this.data = ean;
        this.cargarProductos(false);
    }

    async presentPopover(myEvent, supermercado, urlWeb) {
        const popover = await this.popoverController.create({
            component: AddProductInListComponent,
            componentProps: { supermercado: supermercado, urlWeb : urlWeb },
            translucent: true,
            event: myEvent
        });

        popover.onDidDismiss().then((data : any) => {
            if(data && data.data && data.data.info != "")
                this.presentActionPrompt(data.data.info)
        });

        return await popover.present();
    }

    async presentActionPrompt(fromLine = false) {
        
        if(!fromLine)
            this.masBarato = this.json.masBarato;
        else
            this.masBarato = fromLine;

        const actionSheet = await this.actionSheetController.create({
            header: '¿A que lista deseas añadir el producto?',
            cssClass: 'action-sheets-basic-page',
            buttons: this.buttons
        });
        await actionSheet.present();
    }

    createButtons() {
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.getShopingList(rs.token, false).then(data => {
                this.listas = data;
                for (let index in this.listas) {
                    this.buttons.push({
                        text: this.listas[index].nombre,
                        icon: 'cart',
                        handler: () => {
                            this.addToCart(this.listas[index].nombre);
                            return true;
                        }
                    });
                }
                this.buttons.push({
                    text: 'Añadir nueva lista de la compra',
                    icon: 'add',
                    handler: () => {
                        this.presentAlertPrompt();
                        return true;
                    }
                });
            });
        });
    }

    async presentAlertPrompt() {
        const alert = await this.alertCtrl.create({
            header: 'Nueva lista de la compra',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Nombre'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel'
                },
                {
                    text: 'Crear',
                    handler: data => {
                        this.crearListaCompra(data);
                    }
                }
            ]
        });
        await alert.present();
    }

    async alertCrearListaCompar(titulo, body, result) {
        const alert = await this.alertCtrl.create({
            header: titulo,
            message: body,
            buttons: [
                {
                    text: 'Ok',
                    handler: data => {
                        if (result) {
                            this.buttons = [];
                            this.createButtons();
                        }
                    }
                }
            ]
        });
        await alert.present();
    }

    async crearListaCompra(data) {
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.newShopingList(data, rs.token).then(d => {
                this.result = d;
                if (this.result.error) {
                    this.alertCrearListaCompar("Error", this.result.error.text, false);
                } else if (this.result.ok) {
                    this.alertCrearListaCompar("¡Buen trabajo!", this.result.ok.text, true);
                    this.addToCart(data.name, false);
                    console.log(this.result.ok);
                } else {
                    console.log(this.result);
                }
            });
        });
    }

    addToCart(nombre, loading = true) {
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.addToCart(nombre, this.data, rs.token, this.masBarato, loading).then(data => {
                this.resultAdd = data;
                if (this.resultAdd.ok) {
                    this.presentToast(this.resultAdd.ok.text);
                } else {
                    this.presentToast(this.resultAdd.error.text);
                }
            });
        });
    }

    share() {
        this.socialSharing.share('Revisa los precios de ' + this.json.producto.nombre + '\n', '', "", "https://com.jr91.ahorracompra/product-prices/" + this.data).then(() => {
            // Success!
        }).catch(() => {
            // Error!
        });
    }

    changeFavourite() {
        if (this.json.producto.favorito == null) {
            this.storage.get('USER_INFO').then((rs) => {
                this.rest.isFavourite(this.data, rs.token).then(data => {
                    this.resultFavorite = data;
                    if (this.resultFavorite.ok) {
                        this.json.producto.favorito = Date();
                        this.presentToast(this.resultFavorite.ok.text);
                    } else {
                        this.presentToast(this.resultFavorite.error.text);
                    }
                });
            });
        } else {
            this.storage.get('USER_INFO').then((rs) => {
                this.rest.noIsFavourite(this.data, rs.token).then(data => {
                    this.resultFavorite = data;
                    if (this.resultFavorite.ok) {
                        this.json.producto.favorito = null;
                        this.presentToast(this.resultFavorite.ok.text);
                    } else {
                        this.presentToast(this.resultFavorite.error.text);
                    }
                });
            });
        }
    }

    async presentToast(text) {
        const toast = await this.toastController.create({
            message: text,
            duration: 2000
        });
        toast.present();
    }

    doRefresh(event) {
        this.cargarProductos(event);
    }

    seeFullImage() {
        this.photoViewer.show(this.json.producto.url, this.json.producto.nombre, { share: true });
    }

    goToChart() {
        let navigationExtras: NavigationExtras = {
            state: {
                ean: this.data
            }
        };
        this.router.navigate(['product-chart'], navigationExtras);
    }

    cargarProductos(event, alert = true) {
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.getProductPrices(this.data, rs.token, alert).then(data => {
                this.json = data;                
                if (event !== false)
                    event.target.complete();
                if (this.json.error) {
                    this.presentToast(this.json.error.text);
                    this.router.navigate(['home']);
                } else if (this.json.info) {
                    if(this.nReload == 0)
                        this.intervalCargarProductos();
                    this.nReload++;
                    this.showSpinner = true;
                    if(this.nReload > this.nMaxReload){
                        this.presentToast(this.json.info.text);
                        this.router.navigate(['home']);
                    }
                } else {
                    this.showSpinner = false;
                    this.nReload = this.nMaxReload +1;
                }
            });
        });
    }

    intervalCargarProductos(){
        setInterval(() => { 
            if(this.nMaxReload >= this.nReload)
                this.cargarProductos(false, false); 
        }, 5000);

    }

}
