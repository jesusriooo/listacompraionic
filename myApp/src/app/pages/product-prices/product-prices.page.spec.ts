import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductPricesPage } from './product-prices.page';

describe('ProductPricesPage', () => {
  let component: ProductPricesPage;
  let fixture: ComponentFixture<ProductPricesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductPricesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductPricesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
