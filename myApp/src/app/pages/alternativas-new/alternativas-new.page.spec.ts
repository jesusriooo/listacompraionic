import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AlternativasNewPage } from './alternativas-new.page';

describe('AlternativasNewPage', () => {
  let component: AlternativasNewPage;
  let fixture: ComponentFixture<AlternativasNewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlternativasNewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AlternativasNewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
