import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { RestService } from 'src/app/services/rest/rest.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ToastController, ModalController, AlertController } from '@ionic/angular';
import { AlertModalPage } from '../alert-modal/alert-modal.page';

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})

export class UsersPage{

  json: any;
  subscription: any;
  result: any;
  constructor(public alertController: AlertController, public modalCtrl: ModalController, public toastController: ToastController, public alert: AlertService, private storage: Storage, public rest: RestService, ) {

  }

  getUsers(event) {
    this.storage.get('USER_INFO').then((rs) => {
        this.rest.getUsers(rs.token).then(data => {
            this.json = data;
            if (event !== false)
                event.target.complete();
        });
    });
  }

  cambiarEstadoUsuario(tipo, email){
    this.storage.get('USER_INFO').then((rs) => {
      this.rest.cambiarEstadoUsuario(rs.token, tipo, email).then(data => {
        this.result = data;
        if (this.result.error) {
          this.presentToast(this.result.error.text);
        } else if (this.result.ok) {
          this.getUsers(false);
          this.presentToast(this.result.ok.text);
        }
      });
    });
  }

  async presentAlertConfirm(email) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¡Atención!',
      message: '¿Estás seguro de borrar el usuario?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',          
        }, {
          text: 'Si',
          handler: () => {
            this.borrarUsuario(email);
          }
        }
      ]
    });

    await alert.present();
  }

  borrarUsuario(email){
    this.storage.get('USER_INFO').then((rs) => {
      this.rest.borrarUsuario(rs.token, email).then(data => {
        this.result = data;
        if (this.result.error) {
          this.presentToast(this.result.error.text);
        } else if (this.result.ok) {
          this.getUsers(false);
          this.presentToast(this.result.ok.text);
        }
      });
    });
  }

  async presentAlertPromptEditar(email) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Editar tipo usuario',
      inputs: [
        {
          name: 'radio1',
          type: 'radio',
          label: 'Usuario',
          value: '0',
          checked: true
        },
        {
          name: 'radio2',
          type: 'radio',
          label: 'Colaborador',
          value: '2'
        },
        {
          name: 'radio3',
          type: 'radio',
          label: 'Administrador',
          value: '1'
        },        
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',          
        }, {
          text: 'Editar',
          handler: (data) => {
            this.editarUsuario(data, email);
          }
        }
      ]
    });

    await alert.present();
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        duration: 2000
    });
    toast.present();
  }

  editarUsuario(tipo, email){
    this.storage.get('USER_INFO').then((rs) => {
      this.rest.editarUsuario(rs.token, tipo, email).then(data => {
        this.result = data;
        if (this.result.error) {
          this.presentToast(this.result.error.text);
        } else if (this.result.ok) {
          this.getUsers(false);
          this.presentToast(this.result.ok.text);
        }
      });
    });
  }

  async presentAlertPrompt() {
    let alertModal = this.modalCtrl.create({
      component: AlertModalPage,
      componentProps: {id:1,name:2},
      cssClass: 'small-modal'
    });
    (await alertModal).present();

    const { data } = await (await alertModal).onWillDismiss();
    if(data.dismissed)
      this.getUsers(false);
}
  
  doRefresh(event) {
      this.getUsers(event);
  }

  ionViewWillEnter() {
      this.getUsers(false);
  }
}