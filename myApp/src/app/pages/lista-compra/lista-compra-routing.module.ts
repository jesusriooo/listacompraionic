import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { listaCompraPage } from './lista-compra.page';

const routes: Routes = [
  {
    path: '',
    component: listaCompraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class listaCompraPageRoutingModule {}
