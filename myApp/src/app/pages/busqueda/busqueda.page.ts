import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { RestService } from 'src/app/services/rest/rest.service';
import { NavigationExtras, Router } from '@angular/router';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
    selector: 'app-busqueda',
    templateUrl: './busqueda.page.html',
    styleUrls: ['./busqueda.page.scss'],
})
export class BusquedaPage {

    isItemAvailable = false; // Declare the variable (in this case isItemAvailable) 
    items: any;
    search: any;
    json : any;
    val : any;
    spinerPresent = false;
    textShow = "Introduce el productos a buscar.";

    constructor(private keyboard: Keyboard, private router: Router, private photoViewer: PhotoViewer, private storage: Storage, public rest: RestService) { 
        
        this.keyboard.show();
    }

    inputText(){
        this.spinerPresent = true;
        this.textShow = "";   
    }

    getItems(ev: any) {

        // set val to the value of the searchbar
        this.val = ev.target.value;
        this.spinerPresent = true;
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.getAllProducts(this.val).then(data => {
                this.spinerPresent = false;

                this.json = data;
                this.items = this.json.items;

                if(this.items.length == 0){
                    this.textShow = "Producto no encontrado.";        
                }
                
                if (this.val && this.val.trim() != '') {
                    if(this.items.length > 0)
                        this.textShow = "";
                    this.isItemAvailable = true;
                    this.search = this.items.filter((item) => {
                        return (item.nombre.toLowerCase().indexOf(this.val.toLowerCase()) > -1);
                    })
                } else {
                    this.textShow = "Introduce el productos a buscar.";
                }
            });
        });        
    }

    seeFullImage(url, nombre) {
        this.photoViewer.show(url, nombre, { share: true });
    }

    goToProductInfo(ean) {
        let navigationExtras: NavigationExtras = {
            state: {
                ean: ean
            }
        };
        this.router.navigate(['product-prices'], navigationExtras);
    }

}
