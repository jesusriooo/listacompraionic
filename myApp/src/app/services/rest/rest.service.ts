import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { LoadingController } from '@ionic/angular';
import { AlertService } from '../alert/alert.service';

@Injectable({
    providedIn: 'root'
})
export class RestService {

    apiUrl = 'http://jesusrio.me/api';

    constructor(public alert: AlertService, public loadingController: LoadingController, public http: HttpClient) { }

    async authenticate(id, tipo, email) {
        return new Promise(resolve => {
            this.http.post(this.apiUrl + "/authenticate", { user_id: id, user_tipo: tipo, user_email: email }).subscribe(data => {
                resolve(data);
            }, err => {
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async getAllProducts(val) {
        return new Promise(resolve => {
            this.http.post(this.apiUrl + "/getAllProducts", {'name': val}).subscribe(data => {
                resolve(data);
            }, err => {
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async anadirAlternativa(token, ean1, ean2) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/anadirAlternativa", { ean1: ean1, ean2:ean2 }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async borrarLista(token, name) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/borrarLista", { name: name }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async getProductChart(ean) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            this.http.post(this.apiUrl + "/getProductChart", { ean: ean }).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async newShopingList(data, token) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/newShopingList", { name: data.name }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async getProductPrices(ean, token, alert) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        if(alert)
            await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/getProductInfo", { ean: ean }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async isFavourite(ean, token) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/isFavourite", { ean: ean }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async login(email, password) {
        const loading = await this.loadingController.create({
            message: 'Iniciando sesión...',
        });

        await loading.present();

        return new Promise(resolve => {
            
            this.http.post(this.apiUrl + "/login", { email: email, password: password }).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async registro(email, password) {
        const loading = await this.loadingController.create({
            message: 'Registrando...',
        });

        await loading.present();

        return new Promise(resolve => {
            
            this.http.post(this.apiUrl + "/registro", { email: email, password: password }).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async olvido(email) {
        const loading = await this.loadingController.create({
            message: 'Enviado email...',
        });

        await loading.present();

        return new Promise(resolve => {
            
            this.http.post(this.apiUrl + "/olvido", { email: email }).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async noIsFavourite(ean, token) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/noIsFavourite", { ean: ean }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async addToCart(nombre, ean, token, masBarato, showLoading = true) {
        
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        if(showLoading)
            await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/addToCart", { ean: ean, nombre: nombre, masBarato:masBarato }, options).subscribe(data => {
                if(showLoading)
                    this.loadingController.dismiss();
                resolve(data);
            }, err => {
                if(showLoading)
                    this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async eliminarProducto(token, ean, listaCompra) {
        
        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/eliminarProducto", { ean: ean, nombre: listaCompra }, options).subscribe(data => {
                resolve(data);
            }, err => {
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async anadirProducto(token, ean, listaCompra) {
        
        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/anadirProducto", { ean: ean, nombre: listaCompra }, options).subscribe(data => {
                resolve(data);
            }, err => {
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async menosProducto(token, ean, listaCompra) {
        
        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/menosProducto", { ean: ean, nombre: listaCompra }, options).subscribe(data => {
                resolve(data);
            }, err => {
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async cambiarEstadoUsuario(token, tipo, email) {

        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();
        
        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/cambiarEstadoUsuario", { email: email, tipo: tipo }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async editarUsuario(token, tipo, email) {

        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();
        
        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/editarUsuario", { email: email, tipo: tipo }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async borrarUsuario(token, email) {

        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();
        
        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/borrarUsuario", { email: email }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async crearUsuario(token, email, tipo) {

        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();
        
        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/crearUsuario", { email: email, tipo:tipo }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async activarAlternativas(token, ean, number) {

        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();
        
        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/activarAlternativas", { ean: ean, number:number }, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async getShopingList(token, loadingShow = true) {

        if (loadingShow) {
            const loading = await this.loadingController.create({
                message: 'Obteniendo información...',
            });

            await loading.present();
        }

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/getShopingList", {}, options).subscribe(data => {
                if (loadingShow)
                    this.loadingController.dismiss();
                resolve(data);
            }, err => {
                if (loadingShow)
                    this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async getProductoOfList(token, name, showLoading) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        if(showLoading)
            await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + "/getProductoOfList", {nombre: name}, options).subscribe(data => {
                if(showLoading)
                    this.loadingController.dismiss();
                resolve(data);
            }, err => {
                if(showLoading)
                    this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });
    }

    async getHistorial(token) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + '/getHistorial', null, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });

    }

    async getFavoritos(token) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + '/getFavoritos', null, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });

    }

    async getUsers(token) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + '/getUsers', null, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });

    }

    async getAlternativas(token) {
        const loading = await this.loadingController.create({
            message: 'Obteniendo información...',
        });

        await loading.present();

        return new Promise(resolve => {
            const options = {
                headers: new HttpHeaders().append('Authorization', 'Bearer ' + token),
            }
            this.http.post(this.apiUrl + '/getAlternativas', null, options).subscribe(data => {
                this.loadingController.dismiss();
                resolve(data);
            }, err => {
                this.loadingController.dismiss();
                this.alert.normalAlert(err.message);
                console.log(err);
            });
        });

    }

}
