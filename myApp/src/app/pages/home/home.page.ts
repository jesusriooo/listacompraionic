import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Platform, AlertController, MenuController } from '@ionic/angular';

import { Router, NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { RestService } from 'src/app/services/rest/rest.service';


@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    subscription: any;
    json: any;
    result: any;
    isOpenBarcode = false;

    constructor(public menuCtrl: MenuController, private storage: Storage, private alertCtrl: AlertController, public rest: RestService, private barcodeScanner: BarcodeScanner, private router: Router, private platform: Platform, ) {

        this.storage.get('USER_INFO').then((rs) => {
            if(rs.type == "1"){
                this.menuCtrl.enable(true, 'admin');
                this.menuCtrl.enable(false, 'first');                
            } else {
                this.menuCtrl.enable(false, 'admin');
                this.menuCtrl.enable(true, 'first');
            }
        });
        

    }

    goToList(name) {
        let navigationExtras: NavigationExtras = {
            state: {
                name: name
            }
        };
        this.router.navigate(['lista-compra'], navigationExtras);
    }

    async getShopingList(event) {
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.getShopingList(rs.token).then(data => {
                this.json = data;
                if (event !== false)
                    event.target.complete();
            });
        });
    }

    doRefresh(event) {
        this.getShopingList(event);
    }

    async crearListaCompra(data) {
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.newShopingList(data, rs.token).then(d => {
                this.result = d;
                if (this.result.error) {
                    this.alertCrearListaCompar("Error", this.result.error.text);
                } else if (this.result.ok) {
                    this.alertCrearListaCompar("¡Buen trabajo!", this.result.ok.text, true);
                } else {

                }
            });
        });
    }

    async alertCrearListaCompar(titulo, body, redirect = false) {
        const alert = await this.alertCtrl.create({
            header: titulo,
            message: body,
            buttons: [
                {
                    text: 'Ok',
                    handler: data => {
                        if (redirect)
                            this.getShopingList(false);
                    }
                }
            ]
        });
        await alert.present();
    }

    async presentAlertPrompt() {
        const alert = await this.alertCtrl.create({
            header: 'Nueva lista de la compra',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Nombre'
                }
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    role: 'cancel'
                },
                {
                    text: 'Crear',
                    handler: data => {
                        this.crearListaCompra(data);
                    }
                }
            ]
        });
        await alert.present();
    }

    openCamera() {
        this.barcodeScanner.scan().then(barcodeData => {
            let navigationExtras: NavigationExtras = {
                state: {
                    ean: barcodeData.text
                }
            };
            if (!barcodeData.cancelled)
                this.router.navigate(['product-prices'], navigationExtras);
            else
                this.isOpenBarcode = true;
        }).catch(err => {
            /* ELIMINAR: SOLO DEBUG EN NAVEGADOR */
            let navigationExtras: NavigationExtras = {
                state: {
                    //ean: 8431876267259
                    //ean: 8410066129157
                    //ean: 8076809572897
                    ean : 50001570246 // inventado
                }
            };
            this.router.navigate(['product-prices'], navigationExtras);
            /* FIN ELIMINAR */
        });
    }

    ionViewDidEnter() {
        // Función que saca de la aplicación cuando se pulsa el botón
        // de atrás en la pantalla home.        
        this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
            if (!this.isOpenBarcode){
                navigator['app'].exitApp();
            } else{
                this.isOpenBarcode = false;
                this.getShopingList(false);
            }           
        });
    }

    ionViewWillEnter(){
        this.getShopingList(false);
    }

    ionViewWillLeave() {
        // Cancela la subscripción al botón atrás
        this.subscription.unsubscribe();
    }

    
}
