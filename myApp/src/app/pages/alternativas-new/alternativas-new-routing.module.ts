import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlternativasNewPage } from './alternativas-new.page';

const routes: Routes = [
  {
    path: '',
    component: AlternativasNewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlternativasNewPageRoutingModule {}
