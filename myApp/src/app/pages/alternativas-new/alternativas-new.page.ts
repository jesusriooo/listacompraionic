import { Component } from '@angular/core';
import { RestService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-alternativas-new',
  templateUrl: './alternativas-new.page.html',
  styleUrls: ['./alternativas-new.page.scss'],
})
export class AlternativasNewPage {

  searchText1 : any;
  searchText2 : any;

  json : any;

  items1 : any;
  items2 : any;
  ean1 :any;
  ean2 :any;
  imagen1 = 'https://static.thenounproject.com/png/340719-200.png';
  imagen2 = 'https://static.thenounproject.com/png/340719-200.png';
  nombre1:any;
  nombre2:any;
  result:any;

  isItemAvailable: any;

  constructor(private alertCtrl: AlertController, public rest: RestService, private storage: Storage,) { }

    search1(){
      this.storage.get('USER_INFO').then((rs) => {
        this.rest.getAllProducts(this.searchText1).then(data => {

                this.json = data;
                this.items1 = this.json.items;       
                
                if(this.items1.length == 0){
                  this.isItemAvailable = false;     
                }
                this.isItemAvailable = true;
                     
        });
      });
    }

    async alert(titulo, body, result) {
      const alert = await this.alertCtrl.create({
          header: titulo,
          message: body,
          buttons: [
              {
                  text: 'Ok',                  
              }
          ]
      });
      await alert.present();
  }

    anadirAlternativa(){
      this.storage.get('USER_INFO').then((rs) => {
        this.rest.anadirAlternativa(rs.token, this.ean1, this.ean2).then(data => {               
            this.result = data;
            if (this.result.error) {
                this.alert("Error", this.result.error.text, false);
            } else if (this.result.ok) {
                this.alert("¡Buen trabajo!", this.result.ok.text, true);
            } else {
            }     
        });
      });
    }
    

    addProduct(ean, imagen, nombre){

      if(this.ean1 == '' || typeof this.ean1 == 'undefined'){
        this.ean1 = ean;
        this.imagen1 = imagen;
        this.nombre1 = nombre;
      }else{
        this.ean2 = ean;
        this.imagen2 = imagen;
        this.nombre2 = nombre;
        this.items1 = [];
        this.items2 = [];
      }

    }

}
