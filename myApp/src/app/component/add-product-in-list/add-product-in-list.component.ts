import { Component, OnInit, Input } from '@angular/core';
import { PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { RestService } from 'src/app/services/rest/rest.service';

@Component({
    selector: 'app-add-product-in-list',
    templateUrl: './add-product-in-list.component.html',
    styleUrls: ['./add-product-in-list.component.scss'],
})
export class AddProductInListComponent implements OnInit {
    @Input("supermercado") supermercado;
    @Input("urlWeb") urlWeb;
    result : any;
    hasAction = false;

    constructor(public toastController: ToastController, private popoverController: PopoverController, private storage: Storage, public rest: RestService) {

    }
    ngOnInit(): void {

    }

    addProducto() {
        this.hasAction = true;
        this.popoverController.dismiss({info : this.supermercado});
    }

    goToWeb(url){
        this.hasAction = true;
        if(!url){
            this.presentToast("No existe URL disponible");
        } else {
            window.open(url, '_system', 'location=yes');
            this.popoverController.dismiss();
        }
       
    }

    async presentToast(text) {
        const toast = await this.toastController.create({
            message: text,
            duration: 2000
        });
        toast.present();
    }

    eventFromPopover() {
        this.popoverController.dismiss();
    }
}