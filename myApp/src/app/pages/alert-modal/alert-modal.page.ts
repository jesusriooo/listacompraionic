import { Component } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { RestService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-alert-modal',
  templateUrl: './alert-modal.page.html',
  styleUrls: ['./alert-modal.page.scss'],
})
export class AlertModalPage {

  user = { 'email' : '' , 'tipo' : ''};
  result : any;

  constructor(public toastController: ToastController, public rest: RestService, public modalCtrl: ModalController, private storage: Storage,) { }

  closeModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  createUsuario(){
    this.storage.get('USER_INFO').then((rs) => {
      this.rest.crearUsuario(rs.token, this.user.email, this.user.tipo).then(data => {
        this.result = data;
        if (this.result.error) {
          this.presentToast(this.result.error.text);
        } else if (this.result.ok) {
          this.presentToast(this.result.ok.text);
          this.closeModal();
        }
      });
    });
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        duration: 2000
    });
    toast.present();
  }

}
