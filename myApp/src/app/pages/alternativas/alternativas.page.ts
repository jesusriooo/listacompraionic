import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert/alert.service';
import { RestService } from 'src/app/services/rest/rest.service';
import { Storage } from '@ionic/storage';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-alternativas',
  templateUrl: './alternativas.page.html',
  styleUrls: ['./alternativas.page.scss'],
})
export class AlternativasPage {

  json: any;
  slideOpts: any;
  botonActivar = false;
  result : any;

  constructor(private router: Router, public toastController: ToastController, public alert: AlertService, private storage: Storage, public rest: RestService,) {
    this.slideOpts = {
      slidesPerView: 3
    }

    this.storage.get('USER_INFO').then((rs) => {
      if(rs.type == "1" || rs.type == "2"){
        this.botonActivar = true;
      }
  });
  }

  goToProductInfo(ean) {
    let navigationExtras: NavigationExtras = {
        state: {
            ean: ean
        }
    };
    this.router.navigate(['product-prices'], navigationExtras);
}

  activar(ean, number) {
    this.storage.get('USER_INFO').then((rs) => {
        this.rest.activarAlternativas(rs.token, ean, number).then(data => {
          this.result = data;
          if (this.result.error) {
            this.presentToast(this.result.error.text);
          } else if (this.result.ok) {
            this.getAlternativas(false);
            this.presentToast(this.result.ok.text);
          }
        });
    });
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        duration: 2000
    });
    toast.present();
  }

  getAlternativas(event) {
    this.storage.get('USER_INFO').then((rs) => {
        this.rest.getAlternativas(rs.token).then(data => {
            this.json = data;
            if (event !== false)
                event.target.complete();
        });
    });
  }

  doRefresh(event) {
    this.getAlternativas(event);
  }

  ionViewWillEnter() {
      this.getAlternativas(false);
  }

}
