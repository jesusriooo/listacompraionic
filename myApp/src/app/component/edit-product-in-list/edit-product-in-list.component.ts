import { Component, OnInit, Input } from '@angular/core';
import { PopoverController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { RestService } from 'src/app/services/rest/rest.service';

@Component({
    selector: 'app-edit-product-in-list',
    templateUrl: './edit-product-in-list.component.html',
    styleUrls: ['./edit-product-in-list.component.scss'],
})
export class EditProductInListComponent implements OnInit {

    @Input("ean") ean;
    @Input("listaCompra") listaCompra;
    result : any;
    hasAction = false;

    constructor(public toastController: ToastController, private popoverController: PopoverController, private storage: Storage, public rest: RestService) {

    }
    ngOnInit(): void {

    }

    eliminarProducto() {
        this.hasAction = true;
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.eliminarProducto(rs.token, this.ean, this.listaCompra).then(data => {
                this.eventFromPopover();
                this.result = data;
                if (this.result.ok) {
                    this.presentToast(this.result.ok.text);
                } else {
                    this.presentToast(this.result.error.text);
                }
                
            });
        });
    }

    anadirProducto() {
        this.hasAction = true;
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.anadirProducto(rs.token, this.ean, this.listaCompra).then(data => {
                this.eventFromPopover();
                this.result = data;
                if (this.result.ok) {
                    this.presentToast(this.result.ok.text);
                } else {
                    this.presentToast(this.result.error.text);
                }
                
            });
        });
    }

    menosProducto() {
        this.hasAction = true;
        this.storage.get('USER_INFO').then((rs) => {
            this.rest.menosProducto(rs.token, this.ean, this.listaCompra).then(data => {
                this.eventFromPopover();
                this.result = data;
                if (this.result.ok) {
                    this.presentToast(this.result.ok.text);
                } else {
                    this.presentToast(this.result.error.text);
                }
                
            });
        });
    }

    async presentToast(text) {
        const toast = await this.toastController.create({
            message: text,
            duration: 2000
        });
        toast.present();
    }

    eventFromPopover() {
        this.popoverController.dismiss();
    }
}