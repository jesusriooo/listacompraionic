import { Component, ElementRef, ViewChild } from '@angular/core';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Router } from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform, AlertController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { AuthenticationService } from '../../services/auth/Authentication.service';
import { RestService } from 'src/app/services/rest/rest.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})

export class LoginPage {

    subscription: any;
    passwordtype: string = 'password';
    passeye: string = 'eye';
    isLoggingIn = true;
    olvidado = false;
    result: any;

    @ViewChild('email', { static: false }) email: any;
    @ViewChild('password', { static: false }) password: any;
    @ViewChild('repassword', { static: false }) repassword: any;

    constructor(private alertCtrl: AlertController, public rest: RestService, private authService: AuthenticationService, private fireAuth: AngularFireAuth, private fb: Facebook, private google: GooglePlus, private router: Router, private platform: Platform, ) {

        

    }

    submit() {

        if (!this.validateEmail(this.email.value) && !this.isLoggingIn) {
            this.alertCrearListaCompar("Error", "El email introducido no es correcto.");
            return;
        }


        if (this.olvidado) {
            this.olvido();
        } else if (this.isLoggingIn) {
            this.login();
        } else {

            if (this.password.value != this.repassword.value && !this.isLoggingIn) {
                this.alertCrearListaCompar("Error", "Las contraseñas deben de ser iguales");
                return;
            }

            if (!this.validatePass(this.password.value) && !this.isLoggingIn) {
                this.alertCrearListaCompar("Error", "La contraseña debe incluir mayusculas, minusculas, números y simbolos.");
                return;
            }

            this.registro();
        }

    }

    validateEmail(email) {
        var re = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
        return re.test(email);
    }

    validatePass(pass) {
        var re = /^(?=.*[a-z]){3,}(?=.*[A-Z]){2,}(?=.*[0-9]){2,}(?=.*[!@#$%^&*()--__+=.]){1,}.{8,}$/;
        return re.test(pass);
    }

    login() {

        this.rest.login(this.email.value, this.password.value).then(d => {
            this.result = d;
            if (this.result.error) {
                this.alertCrearListaCompar("Error", this.result.error.text);
            } else if (this.result.ok) {
                this.authService.login(this.result.ok.text, "Email", this.email.value);
            } else {

            }
        });

    }

    olvido() {

        this.rest.olvido(this.email.value,).then(d => {
            this.result = d;
            if (this.result.error) {
                this.alertCrearListaCompar("Error", this.result.error.text);
            } else if (this.result.ok) {
                this.alertCrearListaCompar("Ok", "Revisa la bandeja de entrada de tu correo electronico para actualizar su contraseña.");
                this.isLoggingIn = false;
            } else {

            }
        });

    }
    
    registro() {

        this.rest.registro(this.email.value, this.password.value).then(d => {
            this.result = d;
            if (this.result.error) {
                this.alertCrearListaCompar("Error", this.result.error.text);
            } else if (this.result.ok) {
                this.alertCrearListaCompar("Ok", "La cuenta ha sido registrada, revisa la bandeja de entrada de tu correo electronico para activarla.");
                this.isLoggingIn = true;
            } else {

            }
        });

    }

    async alertCrearListaCompar(titulo, body, redirect = false) {
        const alert = await this.alertCtrl.create({
            header: titulo,
            message: body,
            buttons: [
                {
                    text: 'Ok',
                }
            ]
        });
        await alert.present();
    }

    toggleForm() {
        this.isLoggingIn = !this.isLoggingIn;
        this.olvidado = false;
    }

    setOlvidado() {
        this.toggleForm();
        this.olvidado = !this.olvidado;
    }

    managePassword() {
        if (this.passwordtype == 'password') {
            this.passwordtype = 'text';
            this.passeye = 'eye-off';
        } else {
            this.passwordtype = 'password';
            this.passeye = 'eye';
        }
    }

    fbLogin() {
        this.fb.login(['public_profile', 'email'])
            .then(res => {
                if (res.status === 'connected') {
                    this.getUserDetail(res.authResponse.userID);
                } else {
                }
            })
            .catch(e => console.log('Error logging into Facebook', e));
    }

    getUserDetail(userid: any) {
        this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
            .then(res => {
                console.log(res);
                //this.users = res;
                this.authService.login(res.id, "Facebook", res.email);
            })
            .catch(e => {
                console.log(e);
            });
    }

    gLogin() {
        let params;

        if (this.platform.is('android')) {
            params = {
                'webClientId': '410938566282-9ic07ds8bls9q8scuhboddti9ucm2eqi.apps.googleusercontent.com',
                'offline': true
            }
        } else {
            params = {}
        }

        this.google.login(params)
            .then((response) => {
                const { idToken, accessToken } = response
                this.onLoginSuccess(idToken, accessToken);
            }).catch((error) => {
                console.log(error);
            });
    }

    onLoginSuccess(accessToken, accessSecret) {
        const credential = accessSecret ? firebase.auth.GoogleAuthProvider.credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider.credential(accessToken);
        this.fireAuth.auth.signInWithCredential(credential)
            .then((response) => {
                console.log(response);
                this.authService.login(response.user.uid, "Google", response.user.email);
            }).catch((error) => {
                console.log(error);
            });

    }

    ionViewDidEnter() {
        // Función que saca de la aplicación cuando se pulsa el botón
        // de atrás en la pantalla home.
        this.subscription = this.platform.backButton.subscribe(() => {
            navigator['app'].exitApp();
        });
    }

    ionViewWillLeave() {
        // Cancela la subscripción al botón atrás
        this.subscription.unsubscribe();
    }

}
