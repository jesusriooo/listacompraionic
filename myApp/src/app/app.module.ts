import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

/*
* CUSTOM COMPONENTE
*/

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { HttpClientModule} from "@angular/common/http";
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';

import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { environment } from '../environments/environment';

import { AuthGuard } from './services/auth/auth-guard.service';
import { IonicStorageModule } from '@ionic/storage';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { EditProductInListComponent } from './component/edit-product-in-list/edit-product-in-list.component';
import { AddProductInListComponent } from './component/add-product-in-list/add-product-in-list.component';
import { AuthenticationService } from './services/auth/Authentication.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AlertModalPage } from './pages/alert-modal/alert-modal.page';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, EditProductInListComponent, AddProductInListComponent, AlertModalPage,  ],
  entryComponents: [EditProductInListComponent, AddProductInListComponent, AlertModalPage],
  imports: [
    FormsModule,
    BrowserModule, 
    HttpClientModule, 
    IonicModule.forRoot(), 
    AppRoutingModule, 
    AngularFireModule.initializeApp(environment.config), 
    AngularFireAuthModule,
    IonicStorageModule.forRoot()
  ],
  exports: [],
  providers: [
    Deeplinks,
    StatusBar,
    BarcodeScanner,
    SplashScreen,
    GooglePlus,
    AuthGuard,
    SocialSharing,
    AuthenticationService,
    PhotoViewer,
    Keyboard,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Facebook
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
