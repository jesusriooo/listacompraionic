import { Component } from '@angular/core';

import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Router, NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { RestService } from 'src/app/services/rest/rest.service';

@Component({
    selector: 'app-historial',
    templateUrl: './historial.page.html',
    styleUrls: ['./historial.page.scss'],
})
export class HistorialPage {

    json: any;
    subscription: any;

    constructor(private storage: Storage, private router: Router, private photoViewer: PhotoViewer, public rest: RestService, ) {

    }

    getHistorial(event) {

        this.storage.get('USER_INFO').then((rs) => {
            this.rest.getHistorial(rs.token).then(data => {
                this.json = data;
                if (event !== false)
                    event.target.complete();
            });
        });


    }

    seeFullImage(url, nombre) {
        this.photoViewer.show(url, nombre, { share: true });
    }

    goToProductInfo(ean) {
        let navigationExtras: NavigationExtras = {
            state: {
                ean: ean
            }
        };
        this.router.navigate(['product-prices'], navigationExtras);
    }

    doRefresh(event) {
        this.getHistorial(event);
    }

    ionViewWillEnter() {
        this.getHistorial(false);
    }

}
